#
# Information used in the version notice
#
# Note that here assignment uses = instead of := because we reference
# $(SRC_FILENAME) but it has not been set yet.
# See https://www.gnu.org/software/make/manual/html_node/Flavors.html
REV_DATE = $(shell git log -n 1 --format=%cd --date=format:'%B %Y' $(SRC_FILENAME))
REV_HASH = $(shell git log -n 1 --format=%h $(SRC_FILENAME))

#
# Turn on/off $(TEX) output
#
V ?= 0
ifeq ($(V), 0)
REDIRECT_OUTPUT := 1>/dev/null 2>/dev/null
endif

#
# Specify the basename and calculate the source and output filename
#
BASENAME        := wentao-resume
SRC_FILENAME    := $(addsuffix .tex, $(BASENAME))
OUTPUT_FILENAME := $(addsuffix .pdf, $(BASENAME))

#
# Tell GNU make that we are running inside or outside containers
#
# For local builds, we often do it on the host environment (outside containers)
# and have no $(TEX) available. So we would like to run the executable with the
# help of some one-off containers. For CI builds, things normally live inside
# containers where $(TEX) is ready for direct use.
#
INSIDE_CONTAINER ?= 0
ifeq ($(INSIDE_CONTAINER), 0)
# See https://github.com/blang/latex-docker/blob/master/latexdockercmd.sh
DOCKER_PREFIX := docker run --rm -i -v "$$PWD":/data blang/latex:latest
endif

TEX := latexmk -pdf
TEX := $(DOCKER_PREFIX) $(TEX)

#
# -jobname is a common option accepted by many flavors of $(TEX) that controls
# the filename of intermediate and final output
#
TEXOPTS := -jobname=$(BASENAME)

all: $(OUTPUT_FILENAME)

%.pdf: %.intermediate.tex
	$(TEX) $(TEXOPTS) $^ $(REDIRECT_OUTPUT)

# Substitute the placeholders in the version notice with real information
%.intermediate.tex: %.tex
	cat $^ | sed "s/January 1970/$(REV_DATE)/" | sed "s/deadbeef/$(REV_HASH)/" > $@

clean:
	rm -vf *.intermediate.tex *.aux *.fdb_latexmk *.fls *.log *.out *.pdf
